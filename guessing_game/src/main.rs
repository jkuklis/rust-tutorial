extern crate rand;

use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Gimme sombre input");

    let sec_num = rand::thread_rng().gen_range(1, 101);

    loop {

        let mut guess = String::new();

        io::stdin().read_line(&mut guess)
            .expect("Failed");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("Your guess {}", guess);

        match guess.cmp(&sec_num) {
            Ordering::Less => println!("Small"),
            Ordering::Greater => println!("Big"),
            Ordering::Equal => {
                println!("Win");
                break;
            }
        }
    }
}
